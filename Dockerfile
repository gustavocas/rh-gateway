FROM openjdk:11
VOLUME /tmp
EXPOSE 8765
ADD ./target/rh-gateway-0.0.1-SNAPSHOT.jar rh-gateway.jar
ENTRYPOINT ["java","-jar","/rh-gateway.jar"]
