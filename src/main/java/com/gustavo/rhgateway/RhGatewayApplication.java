package com.gustavo.rhgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
@RibbonClients(defaultConfiguration = RibbonEurekaClientConfig.class)
@EnableEurekaClient
@EnableZuulProxy
@SpringBootApplication
public class RhGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(RhGatewayApplication.class, args);
	}

}
